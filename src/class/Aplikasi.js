const IO = require("./IO");
const Sort = require("./Sort");

class Aplikasi {
  #arrNilai = [];
  #io;

  constructor() {
    this.#arrNilai = [];
    this.nilaiTertinggi = 0;
    this.nilaiTerendah = 0;
    this.nilaiRata2 = 0;
    this.siswaLulus = 0;
    this.siswaTidakLulus = 0;
  }

  mulai() {
    this.#io = new IO();
    console.log("Aplikasi Kalkulasi Daftar Nilai - Kurniawan Cristianto");
    console.log(
      "Daftar Nilai : " +
        (this.#arrNilai.length <= 0 ? "kosong" : this.#arrNilai)
    );

    this.#io.masukkanNilai(this.#arrNilai, () => {
      Sort.bubbleSort(this.#arrNilai);
      this.kalkulasi();
      this.tampilkanHasil();
      this.tutup();
    });
  }

  kalkulasi() {
    this.nilaiTertinggi = this.#arrNilai[this.#arrNilai.length - 1];
    this.nilaiTerendah = this.#arrNilai[0];
    this.cariNilaiRata2();
    this.cariSiswaLulus();
    this.siswaTidakLulus = this.#arrNilai.length - this.siswaLulus;
  }

  cariNilaiRata2() {
    const totalNilai = this.#arrNilai.reduce(
      (total, curr) => +total + +curr,
      0
    );
    const nilaiRata2 = totalNilai / this.#arrNilai.length;
    this.nilaiRata2 = Math.round(nilaiRata2 * 100) / 100;
  }

  cariSiswaLulus() {
    this.siswaLulus = this.#arrNilai.filter((x) => x >= 75).length;
  }

  tampilkanHasil() {
    console.log();
    console.log("Daftar Nilai (setelah diurutkan): " + this.#arrNilai);
    console.log("Nilai tertinggi : " + this.nilaiTertinggi);
    console.log("Nilai terendah : " + this.nilaiTerendah);
    console.log("Nilai Rata-Rata : " + this.nilaiRata2);
    console.log("Jumlah Siswa Lulus : " + this.siswaLulus);
    console.log("Jumlah Siswa Tidak Lulus : " + this.siswaTidakLulus);
    console.log();
  }

  tutup() {
    this.#arrNilai = [];
    console.log("Terima kasih sudah menggunakan aplikasi ini");
    this.#io.bertanyaRestart(() => {
      this.mulai();
    });
  }
}

module.exports = Aplikasi;
