const readline = require("readline");

class IO {
  #rl;

  constructor() {
    this.#rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
  }

  masukkanNilai(arrNilai, cb) {
    console.log("Masukkan Nilai (q untuk melakukan kalkulasi) :");

    this.#rl.on("line", (nilai) => {
      if (nilai === "q") {
        if (arrNilai.length < 1) {
          console.log("Data minimal harus memiliki 1 nilai");
          console.log("Masukkan Nilai (q untuk melakukan kalkulasi) :");
          return;
        }
        this.#rl.pause();
        cb();
        return;
      }

      if (isNaN(nilai) || +nilai < 0 || +nilai > 100) {
        console.log("Nilai tidak valid");
      } else {
        arrNilai.push(nilai);
      }

      console.log("Daftar Nilai : " + arrNilai);
      console.log("Masukkan Nilai (q untuk melakukan kalkulasi) :");
    });
  }

  bertanyaRestart(cb) {
    this.#rl.question(
      "Apakah anda ingin menggunakannya lagi ? (y/n)\n",
      (jawaban) => {
        if (jawaban === "y") {
          this.#rl.close();
          console.log();
          cb();
        } else if (jawaban === "n") {
          this.#rl.close();
          process.exit(0);
        } else {
          this.bertanyaRestart();
        }
      }
    );
  }
}

module.exports = IO;
